class HomeController < ApplicationController

  def index
    @now = Time.now
    @order_count      = Order.count
    @orders           = Order.where.not(status: ORDER_DONE, id: 1).order(:date_to)
    @from             = -3
    @to               = 7
    @groups           = schedule_groups

    @from_m = (set_from_time(1) - @now).to_i/60/60/24 ; 
    @to_m = -(@now - (@now + 2.months).end_of_month).to_i/60/60/24; 
    @step_m = (@to_m - @from_m) / 7 
  end

  def scan
  end

  def schedule_pdf
    @from   = params[:from].to_i - Time.now.wday + 1
    @to     = params[:to].to_i - Time.now.wday
    @groups = schedule_groups
    render pdf: ["Harmonogram", Time.now.strftime("%d-%m")].join('-'), 
           encoding: 'utf8',
           orientation: 'Landscape',
           background: true,
           no_background: false,
           grayscale: false,
           lowquality: false
  end

  def schedule_m_pdf
    @from   = params[:from].to_i
    @to     = params[:to].to_i
    @groups = schedule_groups
    @step   = (@to - @from) / 7
    render pdf: ["Harmonogram", Time.now.strftime("%d-%m")].join('-'), 
           encoding: 'utf8',
           orientation: 'Landscape',
           background: true,
           no_background: false,
           grayscale: false,
           lowquality: false
  end
  private

  def schedule_groups
  	groups = {}
    # item_groups_ending = ItemGroup.includes(:items).includes(:items => [ :orders ])\
    #   .where(items: {status: [ITEM_BUSY, ITEM_RESERVED]})\
    #   .where(orders: {date_to: Time.now + @from.days .. Time.now + @to.days})
    # item_groups_starting = ItemGroup.includes(:items).includes(:items => [ :orders ])\
    #   .where(items: {status: [ITEM_BUSY, ITEM_RESERVED]})\
    #   .where(orders: {date_from: Time.now + @from.days .. Time.now + @to.days})
    # (item_groups_ending + item_groups_starting).each do |item_group|
    
    #where(date_to: Time.now + @from.days .. Time.now + @to.days)
    (
      Order.where(date_from: (Time.now - 2.months)..Time.now)\
      +\
      Order.where(date_to: (Time.now..(Time.now+2.months)))
    ).each do |order|
      # gather order information here
      groups[order.id] = {id: order.id, orders: [order]} if groups[order.id].nil?
      
    end
    return groups
  end

  def set_from_time(n)
    month_wday = (@now - 1.months).beginning_of_month.wday
    puts month_wday

    day_from = (@now - 1.months).beginning_of_month  + (8 - month_wday + 1).days

  end
end
