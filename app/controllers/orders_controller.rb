class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]

  # GET /orders
  def index
    if params[:all]
      @orders = Order.where(status: [ORDER_CREATED, ORDER_WAITING]).where.not(id: 1)
    else
      @orders = Order.where(status: ORDER_CREATED).where.not(id: 1)
    end
  end

  # GET /orders/1
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
    @conflict_orders = {}
  end

  # GET /orders/1/edit
  def edit
  end

  def panel
    if params[:order] && params[:order][:number]
      order = Order.where(number: params[:order][:number]).first
      back_param = params[:order][:back] rescue nil
      redirect_to order_path(order, back: back_param) and return if order
    end
    if params[:back]
      @orders = Order.includes(:items => :item_type).where(status: ORDER_WAITING).where.not(id: 1) + Order.includes(:items => :item_type).where(status: ORDER_CREATED).where(items: {status: [ITEM_BUSY, ITEM_SERVICE]}).where.not(id: 1)
    else
      @orders = Order.includes(:items => :item_type).where(status: ORDER_CREATED).where.not(id: 1) 
    end
  end

  def pdf
    @order = Order.find(params[:order_id])
    render pdf: ["Zlecenie", @order.name, @order.number].join('-'), encoding: 'utf8'   # Excluding ".pdf" extension.
  end

  def pdf_raport
    @order = Order.find(params[:order_id])
    @items = Item.where(ean: params[:order][:number].split(','))
    
    @order.owner = params[:order][:owner] if params[:order][:owner]
    @order.description = params[:order][:description] if params[:order][:description]
    back_param = !params[:order][:back].empty? rescue false

    if back_param && @items.pluck(:ean) & @order.items.where(status: [ITEM_BUSY, ITEM_SERVICE]).pluck(:ean) == @order.items.where(status: [ITEM_BUSY, ITEM_SERVICE]).pluck(:ean)
      @order.status = ORDER_DONE
    elsif !back_param && @items.pluck(:ean) & @order.items.where(status: ITEM_RESERVED).pluck(:ean) == @order.items.where(status: ITEM_RESERVED).pluck(:ean)
      @order.status = ORDER_WAITING
    end

    if @order.save && !back_param
      @items.each do |i| 
        if @order && !@order.items.include?(i)
          @order.items.push(i)
        end
        i.update(status: ITEM_BUSY, owner: @order.owner)
      end
      render pdf: ["Raport", @order.name, @order.number].join('-'), encoding: 'utf8'   # Excluding ".pdf" extension.
    elsif back_param
      @items.each{|i| i.update(status: ITEM_ACTIVE, owner: '')}
      redirect_to orders_path, notice: "Sprzęt został zdany."
    else
      redirect_to @order, notice: 'Coś poszło nie tak'
    end

  end

  # POST /orders
  def create
    @order = Order.new(description: order_params[:description],
                       number: order_params[:number],
                       owner: order_params[:owner],
                       manager: order_params[:manager],
                       name: order_params[:name],
                       date_from: Time.parse(order_params[:date_from]),
                       date_to: Time.parse(order_params[:date_to])
                      )

    if @order.save
      redirect_to orders_path, notice: 'Zlecenie zostało stworzone.'
    else
      render :new
    end
  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(order_params)
      redirect_to @order, notice: 'Order was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /orders/1
  def destroy
    @order.destroy
    redirect_to orders_url, notice: 'Order was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def order_params
    params.require(:order).permit(:date_from, :date_to, :number, :owner, :manager, :name, :description, :item_ids => [], :group_ids => [])
  end

  def dates_conflict? o1, o2
    if (o2.date_from <= o1.date_to && o1.date_to <= o2.date_to) || 
       (o2.date_from <= o1.date_from && o1.date_from <= o2.date_to) ||
       (o1.date_from <= o2.date_to && o2.date_to <= o1.date_to) || 
       (o1.date_from <= o2.date_from && o2.date_from <= o1.date_to)
      true
    else
      false
    end
  end

end
