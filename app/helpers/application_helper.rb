module ApplicationHelper

  def left_menu_items
    [
      {
        name: 'Tablica',
        icon: 'fa fa-fw fa-lightbulb-o',
        href: root_path
      },
      {
        name: 'Wpisy',
        icon: 'fa fa-fw fa-envelope-o',
        href: orders_path(all: true)
      },
    ]
  end

  def status_to_class(item)
    case item.status
    when ITEM_ACTIVE
      'text-default'
    when ITEM_RESERVED
      'text-warning'
    when ITEM_BUSY
      'text-info'
    when ITEM_SERVICE
      'text-danger'
    else
      ''
    end
  end

  def wday_local n
    case ((n-1)%7)+1
    when 1
      "Pon"
    when 2
      "Wt"
    when 3
      "Śr"
    when 4
      "Czw"
    when 5
      "Pt"
    when 6
      "Sob"
    when 7
      "Nd"
    end
  end

  def mday_local n
    case n
    when 1
      "Styczeń"
    when 2
      "Luty"
    when 3
      "Marzec"
    when 4
      "Kwiecień"
    when 5
      "Maj"
    when 6
      "Czerwiec"
    when 7
      "Lipiec"
    when 8
      "Sierpień"
    when 9
      "Wrzesień"
    when 10
      "Październik"
    when 11
      "Listopad"
    when 12
      "Grudzień"
    else
      '---'
    end
  end

  def wday_bgcolor n 
    case ((n-1)%7)+1
    when 6
      "background-color: #f0f0c2;"
    when 7
      "background-color: #ffe5bd;"
    else
      ""
    end
  end

  def calc_colspan group, mday
    if mday == group[:dates].first.mday 
      group[:dates].last.mday - group[:dates].first.mday
    elsif mday > group[:dates].first.mday && mday < group[:dates].last.mday
      -1
    else
      0
    end
  end

  def bar_helper order, i
    if order.date_from.mday == (Time.now + i.days).mday && order.date_to.mday == (Time.now + i.days).mday
      'bar-start bar-end'
    elsif order.date_from.mday == (Time.now + i.days).mday
      'bar-start'
    elsif order.date_to.mday == (Time.now + i.days).mday
      'bar-end'
    else
      'bar-continue'
    end
  end

  def bar_color order, i
    if order.date_from <= (Time.now) && order.date_to >= (Time.now)
      'progress-bar-info'
    elsif order.date_from >= (Time.now)
      'progress-bar-warning'
    else
      'progress-bar-default'
    end
  end

  def bar_color_pdf order, i
    if order.date_from <= (Time.now) && order.date_to >= (Time.now)
      '#99cbe8'
    elsif order.date_from >= (Time.now)
      '#efedb6'
    else
      '#eee'
    end
  end

end
