module OrdersHelper

  def progressbar_color(order)
    colors = ['#5eafa0', '#5893DF', '#da9898', '#d08ed8', '#8ed8b6', '#b9b9b9', '#7bdbe5', '#2eb5a2']
    colors[order.owner.split('').map{|q| q.ord}.sum % colors.count]
  end
end
