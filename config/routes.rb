Rails.application.routes.draw do
  resources :orders do
    get 'pdf'
    post 'pdf_raport'
  end
  resources :item_groups
  resources :item_types
  resources :items do
    get 'service'
    post 'add_history'
  end

  # custom routes
  get 'item_barcodes' => 'items#index_barcodes'
  get 'ajax' => 'items#ajax'
  get 'change_status' => 'items#change_status'
  get 'panel' => 'home#scan'
  get 'schedule' => 'home#schedule_pdf'
  get 'schedule_m' => 'home#schedule_m_pdf'
  get 'orders_panel' => 'orders#panel'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
end
