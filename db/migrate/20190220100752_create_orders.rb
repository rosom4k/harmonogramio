class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :status, default: 0
      t.string :description
      t.string :number
      t.string :name
      t.string :owner
      t.datetime :date_from, null: false
      t.datetime :date_to, null: false
      t.timestamps
    end

  end
end
