# def rl(n)
#   (0...n).map { |_| rand(9).to_s }.join()
# end

# @ean = 0

# def rand_order(date_from, date_to)
#   order = Order.new(
#          name: %w(Wyjazd-integracyjny Sport Transmisja Fakty Wiadomości Uwaga! Dlaczego-ja? Zamówienie-alfanumeryczne Życie-menela Rozmowy-pod-mostem Rozmowy-w-tłoku Tele-auto-konferencja Nagranie-testowe Konferencja-testowa Program-na-żywo).shuffle.first,
#          number: [rand(100).to_s, rand(100).to_s, (rand(25) + 65).chr + (rand(25) + 65).chr].shuffle.join('-'),
#          owner: ['Iwona Górzyńska', 'Janusz Januszowski', 'Dżesika Szczodra-Łagodna', "Bogdan Ciupa", 'Magdalena Wąsko', 'Pan Józek', 'Andrzej Waleczny', 'Ilona Iwona', 'Grażyna Jeżyna', 'Anna Wanna', 'Zuzanna Odpływ-Przepływna', 'Włodek Dorodek'].shuffle.first,
#          date_from: date_from,
#          date_to: date_to,
#   )
#   order
# end


# puts 'Will print:'

# puts 'o for new order.'

# Order.create(name: 'Dummy', number: '000', date_from: Time.new(0), date_to: Time.new(0))
# (1..4).each do |date_range|
#   (1..1).each do
#     r = rand(date_range+1)
#     o = rand_order(Time.now + (12 * r).hours, Time.now + (36 * r).hours)
#     print 'o' if o.save
#   end
# end

# (1..3).each do |date_range|
#   o = rand_order(Time.now - 4.days, Time.now + date_range.days)
#   print 'o' if o.save
#   o.update(status: 1)
# end


# (1..2).each do
#   o = rand_order(Time.now - 2.days, Time.now - 1.days)
#   print 'o' if o.save
#   o.update(status: 1)
# end

# (1..2).each do |q|
#   dates = (-4..4).map{|i| Time.now - (14*i*q).hours}.reverse
#   (1..4).each do |i|
#     o = rand_order(dates[i], dates[i+1])
#     print 'o' if o.save
#     o.update(status: 1)
#   end
# end

puts ''
puts "Created #{Order.count} records."

@i = 1
def inc()
  @i += 1
end
Order.all.each{|q| q.destroy}
Order.create(
  name: "Projekt Kampanii",
  number: inc(),
  owner: 'Paweł Rosa',
  date_from: Time.parse('01-01-2020'),
  date_to: Time.parse('01-02-2020'))
Order.create(
  name: "Projekt Kampanii - testy",
  number: inc(),
  owner: 'Paweł Rosa',
  date_from: Time.parse('03-02-2020'),
  date_to: Time.parse('14-02-2020'))
Order.create(
  name: "Kontener",
  number: inc(),
  owner: 'Adrian Gaj',
  date_from: Time.parse('06-01-2020'),
  date_to: Time.parse('24-01-2020'))
Order.create(
  name: "Kontener - testy",
  number: inc(),
  owner: 'Adrian Gaj',
  date_from: Time.parse('27-01-2020'),
  date_to: Time.parse('31-01-2020'))

Order.create(
  name: "Szablony",
  number: inc(),
  owner: 'Adrian Gaj',
  date_from: Time.parse('10-02-2020'),
  date_to: Time.parse('28-02-2020'))

Order.create(
  name: "Szablony - testy",
  number: inc(),
  owner: 'Adrian Gaj',
  date_from: Time.parse('2-03-2020'),
  date_to: Time.parse('6-03-2020'))

Order.create(
  name: "Wyszukiwarka",
  number: inc(),
  owner: 'Paweł Rosa',
  date_from: Time.parse('10-02-2020'),
  date_to: Time.parse('14-02-2020'))

Order.create(
  name: "Wyszukiwarka - testy",
  number: inc(),
  owner: 'Paweł Rosa',
  date_from: Time.parse('17-02-2020'),
  date_to: Time.parse('21-02-2020'))

Order.create(
  name: "Prezentacje",
  number: inc(),
  owner: 'Paweł Rosa',
  date_from: Time.parse('24-02-2020'),
  date_to: Time.parse('13-03-2020'))

Order.create(
  name: "Testy i poprawki całości",
  number: inc(),
  owner: 'Paweł Rosa',
  date_from: Time.parse('16-03-2020'),
  date_to: Time.parse('08-04-2020'))

Order.create(
  name: "Testy i poprawki całości",
  number: inc(),
  owner: 'Adrian Gaj',
  date_from: Time.parse('16-03-2020'),
  date_to: Time.parse('08-04-2020'))
